#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSqlDatabase>
#include <QtSql>
#include <QSqlQuery>
#include <QSqlTableModel>
#include <QTableView>
#include <QMessageBox>

namespace FUNCTIONS {
    bool createConnection();                                                                            // to DB
    void deleteAll(QString *&name_of_product, QString *&customer, QString *&email, QString *&link);     // delete all pointers for data in inputs
    QString getText(QString& name, QString& name_of_product);                                           // text body in email
    void createMSBWarning(QString);                                                                     // create Warning MessageBox
}

namespace Ui {
    class MainWindow;
}
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    bool refreshTable();    // refresh table view in app

private slots:
    // buttons
    void on_AddCustomer_clicked();
    void on_Update_clicked();
    void on_SendEmails_clicked();

    void clearInputs();

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
